#include <iostream>
#include <cstring>
#include <sstream>
#include "checker.hpp"
#include "task1.hpp"
#include "task2.hpp"
#include "task3.hpp"
#include "task4.hpp"

void task1() {
	INIT_CHECKER

	using namespace Task1;

	// простая реализация валидатора
	struct Simple100KService : ICustomerService {
		[[nodiscard]] bool IsValid(int price) const override {
			return price > 100000;
		}
	} testService;

	Product p1{50};
	Product p2{-50};
	Product p3{150000};
	Product p4{50000};

	CHECK(p1.IsValid() == true);
	CHECK(p2.IsValid() == false);
	CHECK(p3.IsValid(&testService) == true);
	CHECK(p4.IsValid(&testService) == false);
}

void task2() {
	INIT_CHECKER

	using namespace Task2;

	try {
		ImageHelper::Downloader::Download(nullptr);
		CHECK(("expected error", false));
	} catch (const std::exception &err) {
		CHECK(strcmp(err.what(), "invalid url") == 0);
	}

	try {
		int tmp;
		ImageHelper::Downloader::Download(&tmp);
		CHECK(("expected error", false));
	} catch (const std::exception &err) {
		CHECK(strcmp(err.what(), "connection refused") == 0);
	}
}

// простая реализация логгера
struct StringLogger : Task3::ILogger {
	std::stringstream out;

	void Log(const std::string &message) override {
		out << message << ";";
	}
};

void task3() {
	INIT_CHECKER

	using namespace Task3;

	StringLogger logger;

	SmtpMailer mailer(&logger);

	mailer.SendMessage("hello");
	mailer.SendMessage("I from C++ land");
	CHECK(logger.out.str() == "hello;I from C++ land;");

	mailer.SendMessage("Are you?");
	CHECK(logger.out.str() == "hello;I from C++ land;Are you?;");
}

void task4() {
	INIT_CHECKER

	using namespace Task4;

	StringLogger logger;

	AccountEntity account{&logger};
	RoleEntity role{&logger};

	role.SaveToRepository();
	account.SaveToRepository();
	role.SaveToRepository();

	CHECK(logger.out.str() == "Role saved;Account saved;Role saved;");
}

int main() {
	task1();
	task2();
	task3();
	task4();
	return 0;
}
