//
// Created by egor9814 on 24 Feb 2021.
//

#ifndef SELFWORK1_CHECKER_HPP
#define SELFWORK1_CHECKER_HPP

#include <iostream>
#include <string>

/** Небольшой фреймворк для тестирования */

#define LOG(MESSAGE, FILE, LINE, FUNC) \
	std::cout << FILE << ":" << LINE << " `" << FUNC << "`: " << MESSAGE << std::endl

namespace __checker__ {
	struct Checker {
		bool success;
		std::string file, function;
		int line;

		~Checker() {
			if (!success) {
				LOG("test failed", file, line, function);
			}
		}
	};
}

#define INIT_CHECKER __checker__::Checker __checkInstance{true, __FILE__, __FUNCTION__, __LINE__};
#define CHECK(CONDITION) do {\
	if (!(CONDITION)) {\
		LOG("\"" #CONDITION "\" has failed", __FILE__, __LINE__, __FUNCTION__);\
		__checkInstance.success = false;\
	}\
} while(false)
#define REQUIRE(CONDITION) do {\
	if (!CONDITION) {\
		LOG("\"" #CONDITION "\" has failed", __FILE__, __LINE__, __FUNCTION__);\
		__checkInstance.success = false;\
		return;\
	}\
} while(false)

#endif //SELFWORK1_CHECKER_HPP
