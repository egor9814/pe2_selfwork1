//
// Created by egor9814 on 24 Feb 2021.
//

#ifndef SELFWORK1_TASK1_HPP
#define SELFWORK1_TASK1_HPP

/** Задача: Валидация данных
 * Проблема: Валидация данных в проекте.
 * Например, проверка введенного адреса эл. почты,
 * длины имени пользователя, сложности пароля и т.п.
 *
 * class Product
 * {
 * public:
 *   int price;
 *
 *   bool IsValid()
 *   {
 *     return price> 0;
 *   }
 * };
 *
 * Например, объект Product начал использовать CustomerService,
 * который считает валидным продукт с ценой больше 100 тыс. рублей. Что делать?
 * Понятно,что придется изменять наш объект продукта, например:
 *
 * class Product
 * {
 * public:
 *   int price;
 *
 *   bool IsValid(bool isCustomerService)
 *   {
 *     if (isCustomerService == true)
 *       return price > 100000;
 *     return price > 0;
 *   }
 * };
 *
 * Решение: Очевидно, что при дальнейшем использовании объекта Product
 * логика валидации его данных будет изменяться и усложняться.
 * Видимо пора отдать ответственность за валидацию данных продукта другому объекту.
 * Причем надо сделать так, чтобы сам объект продукта независел от конкретной реализации его валидатора.
 * Получаемкод:
 * */

namespace Task1 {
	/** Введем интерфейс сервиса */
	struct ICustomerService {
		virtual ~ICustomerService() = default;
		[[nodiscard]] virtual bool IsValid(int price) const = 0;
	};

	class Product {
	public:
		int price{0};

		/** Используем реализацию сервиса в качестве валидатора
		 * @param customerService реализация сервиса, если он nullptr,
		 *        то цена валидна, если она > 0
		 * @return состояние валидности цены */
		bool IsValid(ICustomerService *customerService = nullptr) const {
			if (customerService)
				return customerService->IsValid(price);
			return price > 0;
		}
	};
}

#endif //SELFWORK1_TASK1_HPP
