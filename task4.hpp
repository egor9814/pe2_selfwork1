//
// Created by egor9814 on 24 Feb 2021.
//

#ifndef SELFWORK1_TASK4_HPP
#define SELFWORK1_TASK4_HPP

namespace Task4 {
	struct IRepository;
	struct IAbstractEntity;

	struct IRepository {
		virtual ~IRepository() = default;
		virtual void Save() = 0;
	};

	struct IAbstractEntity {
		virtual ~IAbstractEntity() = default;

		void SaveToRepository() {
			if (auto repo = newRepository()) {
				repo->Save();
				delete repo;
			}
		}

	protected:
		virtual IRepository *newRepository() = 0;
	};

	struct AccountEntity : IAbstractEntity {
		Task3::ILogger *logger;

		explicit AccountEntity(Task3::ILogger *logger) : logger(logger) {}

	protected:
		struct Repository : IRepository {
			Task3::ILogger *logger;

			explicit Repository(Task3::ILogger *logger) : logger(logger) {}

			void Save() override {
				if (logger) {
					logger->Log("Account saved");
				}
			}
		};

	protected:
		IRepository *newRepository() override {
			return new Repository{logger};
		}
	};

	struct RoleEntity : IAbstractEntity {
		Task3::ILogger *logger;

		explicit RoleEntity(Task3::ILogger *logger) : logger(logger) {}

	protected:
		struct Repository : IRepository {
			Task3::ILogger *logger;

			explicit Repository(Task3::ILogger *logger) : logger(logger) {}

			void Save() override {
				if (logger) {
					logger->Log("Role saved");
				}
			}
		};

		IRepository *newRepository() override {
			return new Repository{logger};
		}
	};

}

#endif //SELFWORK1_TASK4_HPP
