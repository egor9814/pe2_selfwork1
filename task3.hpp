//
// Created by egor9814 on 24 Feb 2021.
//

#ifndef SELFWORK1_TASK3_HPP
#define SELFWORK1_TASK3_HPP

/** Принцип открытости и закрытости
 * Формулировка: программные сущности (классы, модули, функции и т.д.)
 * должны быть открыты для расширения, но закрыты для изменения.
 * Проблема: Пример нарушения принципа открытости/закрытости – использование
 * конкретных объектов без абстракций. Предположим, что у нас есть объект SmtpMailer.
 * Для логирования своих действий он использует Logger, который записывает информацию в
 * текстовые файлы. Рассмотрим соответствующие классы.
 * class Logger
 * {
 * public:
 *   void Log(string logText)
 *   {
 *     // сохранить лог в файле
 *   }
 * };
 * class SmtpMailer
 * {
 * private:
 *   Logger* logger;
 * public:
 *   SmtpMailer()
 *   {
 *     logger = newLogger();
 *   }
 *   void SendMessage(string message)
 *   {
 *     // отправка сообщения
 *   }
 * };
 * Такая конструкция вполне жизнеспособна до тех, пока мы не решим записывать
 * лог SmptMailer'a в базу данных. Для этого нужно создать класс,
 * который будет записывать все «логи» не в текстовый файл, а в базу данных: Например.
 * class DatabaseLogger
 * {
 * public:
 *   void Log(string logText)
 *   {
 *     // сохранить лог в базе данных
 *   }
 * };
 * Теперь нужно изменить класс SmptMailerиз-за изменившегося требования:
 * class SmtpMailer
 * {
 * private:
 *   DatabaseLogger* logger;
 * public:
 *   SmtpMailer()
 *   {
 *     logger = newDatabaseLogger();
 *   }
 *   void SendMessage(string message)
 *   {
 *     // отправка сообщения
 *   }
 * };
 * Но, по принципу единственности ответственности не SmptMailer отвечает за логирование,
 * почему изменения дошли и донего? Потому что нарушен наш принцип открытости/закрытости.
 * SmptMailer не закрыт для модификации. Пришлось его изменить, чтобы поменять
 * способ хранения его логов.
 * Решение: В данном случае защитить SmtpMailer поможет выделение абстракции.
*/

namespace Task3 {
	/// Интерфейс логгера
	struct ILogger {
		virtual ~ILogger() = default;
		virtual void Log(const std::string &message) = 0;
	};

	class SmtpMailer {
		ILogger *logger;
	public:
		explicit SmtpMailer(ILogger *logger = nullptr) : logger(logger) {}

		void setLogger(ILogger *logger) {
			this->logger = logger;
		}

		void resetLogger() {
			logger = nullptr;
		}

		[[nodiscard]] ILogger *getLogger() const {
			return logger;
		}

		void SendMessage(const std::string &message) {
			if (logger) {
				logger->Log(message);
			}
		}
	};
}

#endif //SELFWORK1_TASK3_HPP
