//
// Created by egor9814 on 24 Feb 2021.
//

#ifndef SELFWORK1_TASK2_HPP
#define SELFWORK1_TASK2_HPP

/** Проблема
 * Нарушения принципа единственности ответственности – God object.
 * Этот объект знает и умеет делать все, что только можно.
 * Рассмотрим на примере класс ImageHelper.
 *
 * class ImageHelper
 * {
 * public:
 *   static void Save(Image image){
 *     // сохранение изображение в файловой системе
 *   }
 *   static int DeleteDuplicates(){
 *     // удалить из файловой системы все дублирующиеся изображения и вернуть количество удаленных
 *   }
 *   static Image SetImageAsAccountPicture(Image image, Account account) {
 *     // запрос к базе данных для сохранения ссылки на это изображение для пользователя
 *   }
 *   static Image Resize(Image image, int height, int width){
 *     // изменение размеров изображения
 *   }
 *   static Image InvertColors(Image image){
 *     // изменить цвета на изображении
 *   }
 *   static byte* Download(Url imageUrl){
 *     // загрузка битового массива с изображением с помощью HTTP запроса
 *   }
 *   // и т.п.
 * };
 *
 * Каждая ответственность этого класса ведет к его потенциальному изменению.
 * Получается, что этот класс будет очень часто менять свое поведение,
 * что затруднит его тестирование и тестирование компонентов,
 * которые его используют. Такой подход снизит работоспособность системы
 * и повысит стоимость ее сопровождения.
 * Решение: Решением является разделить этот класс по принципу единственности ответственности:
 * один класс на одну ответственность.
*/

namespace Task2 {
	using byte = unsigned char;
	// для решения задачи не обязательно иметь структуру класса
	using Image = void*;
	using Account = void*;
	using Url = void*;

	namespace ImageHelper {
		class Saver {
		public:
			/// сохранение изображение в файловой системе
			static void Save(Image image);
		};

		class DuplicateDeleter {
		public:
			/// удалить из файловой системы все дублирующиеся изображения и вернуть количество удаленных
			static int Apply();
		};

		class AccountPicture {
		public:
			/// запрос к базе данных для сохранения ссылки на это изображение для пользователя
			static Image SetFor(Account account, Image image);
		};

		class Resizer {
		public:
			/// изменение размеров изображения
			static Image Apply(Image image, int height, int width);
		};

		class ColorInverter {
		public:
			/// изменить цвета на изображении
			static Image Apply(Image image);
		};

		class Downloader {
		public:
			/// загрузка битового массива с изображением с помощью HTTP запроса
			static byte *Download(Url imageUrl) {
				// пусть для теста здесь будет реализация
				if (imageUrl)
					throw std::runtime_error("connection refused");
				throw std::runtime_error("invalid url");
			}
		};
	}
}

/** P.S. Реализацию можно разделить на разные файлы */

#endif //SELFWORK1_TASK2_HPP
